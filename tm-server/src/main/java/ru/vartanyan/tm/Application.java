package ru.vartanyan.tm;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.util.SystemUtil;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="yourRootElementName")

public class Application {

    public static void main(String[] args) throws Throwable {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
