package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    void clear(@NotNull final String userId);

    @Nullable
    List<E> findAll(@NotNull final String userId);

    @Nullable
    E findById(@NotNull final String id,
               @NotNull final String userId);

    void removeById(@NotNull final String id,
                    @NotNull final String userId);

    @Nullable
    E findOneByIndex(@NotNull Integer index,
                     @NotNull String userId) throws Exception;

    @Nullable
    E findOneByName(@NotNull final String name,
                    @NotNull final String userId) throws Exception;

    void removeOneByIndex(@NotNull final Integer index,
                          @NotNull final String userId) throws Exception;

    void removeOneByName(@NotNull final String name,
                         @NotNull final String userId) throws Exception;

    @Nullable
    List<E> findAll(@NotNull final Comparator<E> comparator,
                    @NotNull final String userId);

    void showEntity(@NotNull final E entity);

}
