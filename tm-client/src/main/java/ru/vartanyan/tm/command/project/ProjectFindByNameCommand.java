package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.endpoint.Project;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectFindByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-find-by-name";
    }

    @Override
    public String description() {
        return "Find project by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW PROJECT]");
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final Session session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final Project project = endpointLocator.getProjectEndpoint().findProjectByName(name, session);
    }

}
