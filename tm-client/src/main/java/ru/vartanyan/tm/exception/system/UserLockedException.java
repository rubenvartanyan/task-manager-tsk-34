package ru.vartanyan.tm.exception.system;

public class UserLockedException extends Exception{

    public UserLockedException() throws Exception {
        super("Error! Your account is locked...");
    }

}
